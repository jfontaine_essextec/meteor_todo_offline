
# Meteor

### Installation
* Linux/Mac
	* curl https://install.meteor.com/ | sh
* Windows 
	* https://install.meteor.com/windows

### Getting started
* Create project
	* meteor create project-name

### Security 
* removing insecure
	* meteor remove insecure


### Testing

#### install testing package
* meteor add practical meteor:mocha

#### Run tests 
* meteor test --driver-package practicalmeteor:mocha

#### Caveats
* It when testing Meteor.user() doesn't work 
	* it seems that this.userId is the solution
	* https://stackoverflow.com/questions/32800211/meteor-why-should-i-use-this-userid-over-meteor-userid-whenever-possible

#### Explanation
* describe is used to state what you're testing
	* good for grouping together different types of test
	* ex: outer describes can be it's file or class
	* ex: inner describes can be it's functions
* it is the description of what should be happening


#### Links
* https://mochajs.org/
* https://www.meteor.com/tutorials/blaze/testing
* https://www.youtube.com/watch?v=MLTRHc5dk6s
	

# Working with GroundDB

### Important Links
* https://subvisual.co/blog/posts/45-offline-web-apps-with-meteor/
* https://github.com/GroundMeteor/db
* https://atmospherejs.com/ground/db
* http://blog.capsulecat.com/2016/02/09/local-collection-in-meteor/


### What I did
* init Ground collection
	* const tasks = new Ground.Collection('tasks'); 
		* This init's a Meteor.Collection object

* Pure client-side offline database
	* tasks = new Ground.Collection('tasks', { connecion: null });
		* this actually worked but is buggy


### Fun facts
* information seems to update when i kill the server
* since connection is null make it doesn't seem to be a way to update the information

### Cavets 
* Unpredicted behavior
	* It seems that among users the data gets publishing stops working locally
	* when the conneciton is set as null it seems to not get any updates from the sever 
	  in return  it seems it works primaryly on the data it gets from last publication
* Users require online
	* the users would don't seem to work offline


### Test Cases 
* Killed server while running meteor with grounddb
	* data is saved locally
	* data syncs up when internet is active
* Killed server then closed the browser tab then started the server and restarted it
	* it seems that the data doesn't sync after the tab has been closed
	* if the tab is open and server is killed data is synced once the database has been synced
* Deployed Application to AWS
	* Turned internet off on computer logged in on phone executed task (insert, delete, update)
		* viewed updates from the phone



