

import { Meteor } from 'meteor/meteor';
import { Random } from 'meteor/random';
import { Accounts }  from 'meteor/accounts-base';
import { assert } from 'meteor/practicalmeteor:chai'

faker =  require('faker');

import { Tasks } from './tasks.js';

if (Meteor.isServer) {

	describe('Tasks', () => {
		describe('methods', () => {
			let userId;
			let taskId;

			// runs before the test
			beforeEach( () => {
				Tasks.remove({});

				userId = Accounts.createUser({
					email: 'faker@fake.com',
					username: 'faker',
					password: '123456'
				});

				taskId = Tasks.insert({
					text: 'test task',
					createAt: new Date(),
					owner: userId,
					username: ' tmeasday',
				});
			});

			it('can delete owned task', () => {

				// find the internal implementation of the task method so we
				// can test it in isolation
				const deleteTask = Meteor.server.method_handlers['tasks.remove'];

				// set up a fake method invoncation that looks like what the method expects
				const invocation = { userId };

				deleteTask.apply(invocation, [taskId]);

				// console.log(Tasks.);
				assert.equal(Tasks.find().count(), 0);

			});
		});

		describe('crud_ops', () => {
			
			let userId;
			let text = faker.lorem.sentence();

			beforeEach( () => {
				// clears the entire db
				Tasks.remove({});
				Meteor.users.remove({});

				// Creates new User
				console.log('Do something');
				userId = Accounts.createUser({
					email: 'faker@fake.com',
					username: 'faker',
					password: '123456'
				});
			});

			afterEach( () => {
				// removes all created users
				Meteor.users.remove({});
			});

			it('Inserted task', () => {

				const insertTask = Meteor.server.method_handlers['tasks.insert'];
				const invocation = { userId };

				insertTask.apply(invocation, [text]);

				let doc = Tasks.findOne({});

					assert.equal(doc.text, text);
			});

			describe('Update Task', () => {

				let taskId;

				beforeEach( () => {
					// clears the entire db
					Tasks.remove({});
					Meteor.users.remove({});

					// Creates new User
					console.log('Do something');
					userId = Accounts.createUser({
						email: 'faker@fake.com',
						username: 'faker',
						password: '123456'
					});

					taskId = Tasks.insert({
						text: faker.lorem.sentence(),
						createAt: new Date(),
						owner: userId,
						username: Meteor.users.findOne(userId).username,
					});
				});

				afterEach( () => {
					Tasks.remove({});
					Meteor.users.remove({});
				} );


				it('should updated task to private', () => {

					const setPrivateTask = Meteor.server.method_handlers['tasks.setPrivate'];

					const invocation = { userId };

					setPrivateTask.apply(invocation, [taskId,true]);

					task = Tasks.findOne(taskId);

					assert.isTrue(task.private);

				});

				it('should update task to be checked', () => {

					const setChecked = Meteor.server.method_handlers['tasks.setChecked'];

					const invocation = { userId };

					setChecked.apply(invocation, [taskId,false]);

					task = Tasks.findOne(taskId);

					assert.isFalse(task.checked);

				});
			});

		

		});

	});
}