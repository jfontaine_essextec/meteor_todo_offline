import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { Ground } from 'meteor/ground:db';

import { check } from 'meteor/check';



// export const Tasks = new Mongo.Collection('tasks');
export const Tasks = new Ground.Collection('tasks');
// const tasks = new Mongo.Collection('tasks');
// Tasks.clear();
// Ground.Collection(Meteor.users);

if (Meteor.isServer){

	Meteor.publish('tasks', function tasksPublication(){
		// console.log('Does this work');
		return Tasks.find({
			$or: [
				{ private: { $ne: true } },
				{ owner: this.userId },
			]
		});
	});
}


Meteor.methods({
	'tasks.insert'(text){
		check(text, String);

		if (! this.userId){
			throw new Meteor.Error('not-auth');
		}

		// console.log(Ground);
		Tasks.insert({
			text,
			createdAt: new Date(),
			owner: this.userId,
			username: Meteor.users.findOne(this.userId).username
		});
	},

	'tasks.remove'(taskId) {
		check(taskId, String);

		const task = Tasks.findOne(taskId);

		if (task.owner !== this.userId){
			throw new Meteor.Error('not-auth');
		} 

		Tasks.remove(taskId);
	},
	'tasks.setChecked'(taskId, setChecked) {
		check(taskId, String);
		check(setChecked, Boolean);

		const task = Tasks.findOne(taskId);
		if (task.private && task.owner !== this.userId){
			throw new Meteor.Error('not-auth');
		} 

		Tasks.update(taskId, { $set :{checked: setChecked} } );
	},
	'tasks.setPrivate'(taskId,setToPrivate) {
		check(taskId, String);
		check(setToPrivate, Boolean);

		const task = Tasks.findOne(taskId);
		// console.log(task);
		// console.log(task.private);
		if(task.owner !== this.userId) {
			throw new Meteor.Error('not-auth');
		}

		Tasks.update(taskId, { $set : { private: setToPrivate } });
	},

});









