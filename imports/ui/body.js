import { Template } from 'meteor/templating';
import { ReactiveDict } from 'meteor/reactive-dict';

import { Tasks } from '../api/tasks.js';

import './task.js';
import './body.html';

//onCreated calls function when template is created
Template.body.onCreated(function bodyOnCreated() {
	this.state = new ReactiveDict();
	// best place to place subscriptionss
	Meteor.subscribe('tasks');

});

//ReactiveDicts are reactive data stores for client
//

Template.body.helpers({
	tasks() {
		const instance = Template.instance();
		if(instance.state.get('hideCompleted')){
			return Tasks.find( {checked : {$ne : true} }, {sort: {createdAt: -1} });
		}
		return Tasks.find({}, { sort: {createdAt : -1} })
	},
	incompleteCount() {
		return Tasks.find({ checked: {$ne: true} }).count();
	},
});

// binds the events to the particular tast
Template.body.events({
	'submit .new-task'(event){
		event.preventDefault();

		// get value from form element
		const target = event.target;
		const text = event.target.text.value;

		// console.log(text);

		// insert item into the database
		Meteor.call('tasks.insert',text);
		

		target.text.value = '';
	},
	'change .hide-completed input'(event, instance){
		instance.state.set('hideCompleted', event.target.checked);
	}
});